# paroxysm-ng

Quote bot software for some small internet communities (second edition).

## Building

[Steel Bank Common Lisp (SBCL)](http://sbcl.org/) and
[Quicklisp](https://www.quicklisp.org/beta/) need to be installed. You also
need [SQLite](https://www.sqlite.org/index.html).

Clone [my fork of `birch`](https://git.eta.st/eta/birch) into your Quicklisp
local projects directory:

```
$ cd ~/quicklisp/local-projects/ # might need to create this if it doesn't exist
$ git clone https://git.eta.st/eta/birch
```

Then, run `make` to build the main binary, and `make migrate` to build the
migration utility.

## Running

Copy `config.example.ini` to `config.ini` and modify the values to suit your
needs.

Run with

```
$ ./paroxysm-ng /path/to/config.ini
```

It might crash a lot. Use `Restart=always` or similar.

## Migrating from paroxysm v1

Copy `migrate.example.ini` to `migrate.ini` and put in the PostgreSQL
connection details.

Run with

```
$ ./paroxysm-ng-migrator /path/to/migrate.ini
```

## License

MIT
