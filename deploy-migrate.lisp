(format t "==> Loading Quicklisp~%")
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (unless quicklisp-init
    (error "Quicklisp not installed."))
  (load quicklisp-init))
(format t "==> Loading Quicklisp dependencies~%")
(ql:quickload '(bordeaux-threads sqlite birch cl-ppcre drakma cl-ini postmodern) :silent t)
(format t "==> Loading paroxysm-ng migrator~%")
(load "sqlite.lisp")
(load "stuff.lisp")
(load "migrate.lisp")
(format t "==> Creating executable~%")
(sb-ext:save-lisp-and-die "./paroxysm-ng-migrator" :toplevel #'paroxysm/migrate::main :executable t)
