CREATE TABLE quotes (
  id INTEGER PRIMARY KEY,
  channel TEXT NOT NULL,
  subject TEXT NOT NULL,
  created_ts INT NOT NULL,
  quote TEXT NOT NULL,
  creator TEXT NOT NULL
);
