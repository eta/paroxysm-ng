.PHONY: all migrate

all:
	sbcl --noinform --no-sysinit --no-userinit --disable-debugger --script deploy.lisp

migrate:
	sbcl --noinform --no-sysinit --no-userinit --disable-debugger --script deploy-migrate.lisp
