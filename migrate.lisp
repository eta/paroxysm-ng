(defpackage :paroxysm/migrate
  (:use :cl))

(in-package :paroxysm/migrate)

(defun fetch-keyword-list ()
  (pomo:query (:select 'id 'name 'chan :from 'keywords)))

(defun fetch-quotes-for-keyword (keyword-id)
  (pomo:query (:order-by
               (:select 'text 'creation_ts 'created_by
                :from 'entries
                :where (:= 'keyword_id '$1))
               (:asc 'id))
              keyword-id))

(defun migrate-keyword (keyword keyword-id chan)
  (format t "==> Migrating quotes for ~A in ~A: " keyword chan)
  (loop
    for (quote ts created-by) in (fetch-quotes-for-keyword keyword-id)
    for i from 1
    do (paroxysm::insert-quote chan keyword quote created-by ts)
    finally (format t "did ~A quotes~%" i)))

(defun do-migration ()
  (format t "==> Migrating quote database~%")
  (loop
    for (id name chan) in (fetch-keyword-list)
    do (migrate-keyword name id chan)))

(defun load-config (file)
  (let* ((config (cl-ini:parse-ini file))
         (database (or (cl-ini:ini-value config :database)
                       (error "specify 'database' in configuration")))
         (pg-server (or (cl-ini:ini-value config :pg-server)
                     (error "specify 'pg-server' in configuration")))
         (pg-database (or (cl-ini:ini-value config :pg-database)
                          (error "specify 'pg-database' in configuration")))
         (pg-username (or (cl-ini:ini-value config :pg-username)
                     (error "specify 'pg-username' in configuration")))
         (pg-password (or (cl-ini:ini-value config :pg-password)
                          (error "specify 'pg-password' in configuration")))
         (pg-port (or (cl-ini:ini-value config :pg-port)
                      5432)))
    (tq::connect-database database)
    (unless (paroxysm::database-has-schema-p)
      (error "database has no 'quotes' table: import schema.sql into it"))
    (pomo:connect-toplevel pg-database pg-username pg-password pg-server :port pg-port)))

(defun main ()
  (when (< (length sb-ext:*posix-argv*) 2)
    (format *error-output* "fatal: a path to the config file must be provided~%")
    (format *error-output* "usage: ~A CONFIG_FILE~%" (elt sb-ext:*posix-argv* 0))
    (sb-ext:exit :code 2 :abort t))
  (let ((config-file (elt sb-ext:*posix-argv* 1)))
    (format t "[*] paroxysm-ng migrator / an eta project <https://eta.st>~%")
    (format t "[+] using configuration at ~A~%" config-file)
    (load-config config-file)
    (do-migration)))
