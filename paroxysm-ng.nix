{ pkgs, stdenv, ... }:

let
  sbcl' = pkgs.sbcl.withPackages (ps: with ps; [ bordeaux-threads sqlite birch cl-ppcre drakma cl-ini ]);
  buildLisp = pkgs.writeText "build.lisp" ''
    (load (sb-ext:posix-getenv "ASDF"))
    (loop for sys in '(bordeaux-threads sqlite birch cl-ppcre drakma cl-ini) do (asdf:load-system sys))
    (load "sqlite.lisp")
    (load "stuff.lisp")
    (sb-ext:save-lisp-and-die "./paroxysm-ng" :toplevel #'paroxysm::main :executable t)
  '';
in
stdenv.mkDerivation {
  name = "paroxysm-ng";
  src = pkgs.nix-gitignore.gitignoreSource [] ./.;
  buildInputs = with pkgs; [ sbcl' makeWrapper sqlite.out openssl.out ];

  unpackPhase = ''
    cp -r $src/* .
  '';

  buildPhase = ''
    ${sbcl'}/bin/sbcl --disable-debugger --load "${buildLisp}"
  '';

  installPhase = ''
    mkdir -p $out/bin/
    mv ./paroxysm-ng $out/bin/

    wrapProgram $out/bin/paroxysm-ng --set LD_LIBRARY_PATH "${pkgs.sqlite.out}/lib/:${pkgs.openssl.out}/lib/"
  '';

  dontStrip = true;
}
